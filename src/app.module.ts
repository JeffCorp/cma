import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ContractsService } from './contracts/contracts.service';
import { PatientsService } from './patients/patients.service';
import { ContractsController } from './contracts/contracts.controller';
import { PatientsController } from './patients/patients.controller';
import { ContractsModule } from './contracts/contracts.module';
import { PatientsModule } from './patients/patients.module';
import { HospitalsService } from './hospitals/hospitals.service';
import { HospitalsController } from './hospitals/hospitals.controller';
import { HospitalsModule } from './hospitals/hospitals.module';

const envModule = ConfigModule.forRoot({
  isGlobal: true,
});

@Module({
  imports: [
    envModule,
    MongooseModule.forRoot(process.env.MONGO_DB_URI),
    ContractsModule,
    PatientsModule,
    HospitalsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
