import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HospitalsController } from './hospitals.controller';
import { HospitalsService } from './hospitals.service';
import { HospitalsSchema } from './hospitals.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Hospital', schema: HospitalsSchema }]),
  ],
  controllers: [HospitalsController],
  providers: [HospitalsService],
})
export class HospitalsModule {}
