import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type HospitalsDocument = Hospital & Document;

@Schema()
export class Hospital {
  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  location: string;
}

export const HospitalsSchema = SchemaFactory.createForClass(Hospital);
