import { Controller, Get, Post, Request } from '@nestjs/common';
import { HospitalsService } from './hospitals.service';

@Controller('hospitals')
export class HospitalsController {
  constructor(private hospitalsService: HospitalsService) {}

  @Post()
  async createHospital(@Request() req) {
    return this.hospitalsService.create(req.body);
  }

  @Get()
  async findAllHospitals() {
    return this.hospitalsService.findAll();
  }
}
