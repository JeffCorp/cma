import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv';
import { HospitalsController } from './hospitals.controller';
import { HospitalsService } from './hospitals.service';
import { HospitalsSchema } from './hospitals.model';

dotenv.config({ path: '.env' });

describe('HospitalsController', () => {
  let hospitalsController: HospitalsController;
  let hospitalsService: HospitalsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({
          useFactory: () => ({
            uri: process.env.MONGO_DB_URI,
            connectionFactory: (connection) => {
              connection.plugin(require('mongoose-autopopulate'));

              return connection;
            },
          }),
        }),
        MongooseModule.forFeature([
          { name: 'Hospital', schema: HospitalsSchema },
        ]),
      ],
      controllers: [HospitalsController],
      providers: [HospitalsService],
    }).compile();

    hospitalsController =
      moduleRef.get<HospitalsController>(HospitalsController);
    hospitalsService = moduleRef.get<HospitalsService>(HospitalsService);
  });

  describe('createHospitals', () => {
    it('should call hospitalsService.create with the request body', async () => {
      const req = {
        body: {
          name: 'Hospital A',
          location: 'City X',
        },
      };
      const createdHospital = {
        name: 'Hospital A',
        location: 'City X',
      };
      jest.spyOn(hospitalsService, 'create').mockResolvedValue(createdHospital);

      const result = await hospitalsController.createHospital(req);

      expect(hospitalsService.create).toHaveBeenCalledWith(req.body);
      expect(result).toBe(createdHospital);
    });
  });

  describe('getHospitals', () => {
    it('should call hospitalsService.findAll and return the result', async () => {
      const allHospitals = [
        { _id: 'hospitalId123', name: 'Hospital A', location: 'City X' },
        { _id: 'hospitalId456', name: 'Hospital B', location: 'City Y' },
        // Other hospitals
      ];
      jest.spyOn(hospitalsService, 'findAll').mockResolvedValue(allHospitals);

      const result = await hospitalsController.findAllHospitals();

      expect(hospitalsService.findAll).toHaveBeenCalled();
      expect(result).toBe(allHospitals);
    });
  });
});
