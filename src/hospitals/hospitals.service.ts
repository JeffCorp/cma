import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Hospital } from './hospitals.model';

@Injectable()
export class HospitalsService {
  constructor(
    @InjectModel('Hospital') private hospitalsService: Model<Hospital>,
  ) {}

  async create(hospitalsData: Hospital): Promise<Hospital> {
    const createdHospitals = new this.hospitalsService(hospitalsData);
    return createdHospitals.save();
  }

  async findAll(): Promise<Hospital[]> {
    return this.hospitalsService.find();
  }
}
