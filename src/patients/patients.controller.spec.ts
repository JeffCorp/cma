import { Test, TestingModule } from '@nestjs/testing';
import { PatientsController } from './patients.controller';
import { PatientsService } from './patients.service';

describe('PatientsController', () => {
  let patientsController: PatientsController;
  let patientsService: PatientsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [PatientsController],
      providers: [
        {
          provide: PatientsService,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    patientsController = moduleRef.get<PatientsController>(PatientsController);
    patientsService = moduleRef.get<PatientsService>(PatientsService);
  });

  describe('createPatient', () => {
    it('should call patientsService.create with the request body', async () => {
      const req = {
        body: {
          name: 'Patient A',
          age: 30,
          cancerStage: 'Stage 2',
          treatmentStart: '2023-07-25',
          product: 'ABC vial 10mg/ml',
        },
      };
      const createdPatient = {
        name: 'Patient A',
        age: 30,
        cancerStage: 'Stage 2',
        treatmentStart: '2023-07-25',
        product: 'ABC vial 10mg/ml',
      };
      jest.spyOn(patientsService, 'create').mockResolvedValue(createdPatient);

      const result = await patientsController.createPatient(req);

      expect(patientsService.create).toHaveBeenCalledWith(req.body);
      expect(result).toBe(createdPatient);
    });
  });

  describe('getPatients', () => {
    it('should call patientsService.findAll and return the result', async () => {
      const allPatients = [
        {
          name: 'Patient A',
          age: 30,
          cancerStage: 'Stage 2',
          treatmentStart: '2023-07-25',
          product: 'ABC vial 10mg/ml',
        },
        {
          name: 'Patient B',
          age: 45,
          cancerStage: 'Stage 1',
          treatmentStart: '2023-07-25',
          product: 'ABC vial 10mg/ml',
        },
      ];
      jest.spyOn(patientsService, 'findAll').mockResolvedValue(allPatients);

      const result = await patientsController.getPatients();

      expect(patientsService.findAll).toHaveBeenCalled();
      expect(result).toBe(allPatients);
    });
  });

  describe('getPatient', () => {
    it('should call patientsService.findOne with the provided id', async () => {
      const patientId = 'patientId123';
      const foundPatient = {
        name: 'Patient A',
        age: 30,
        cancerStage: 'Stage 2',
        treatmentStart: '2023-07-25',
        product: 'ABC vial 10mg/ml',
      };
      jest.spyOn(patientsService, 'findOne').mockResolvedValue(foundPatient);

      const param = { id: patientId };
      const result = await patientsController.getPatient(param.id);

      expect(patientsService.findOne).toHaveBeenCalledWith(patientId);
      expect(result).toBe(foundPatient);
    });
  });
});
