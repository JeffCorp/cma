import { Controller, Get, Param, Post, Request } from '@nestjs/common';
import { PatientsService } from './patients.service';

@Controller('patients')
export class PatientsController {
  constructor(private patientsService: PatientsService) {}

  @Post()
  async createPatient(@Request() req) {
    return this.patientsService.create(req.body);
  }

  @Get()
  async getPatients() {
    return this.patientsService.findAll();
  }

  @Get(':id')
  async getPatient(@Param() param) {
    return this.patientsService.findOne(param);
  }
}
