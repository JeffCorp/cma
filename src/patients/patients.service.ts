import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Patient } from './patients.model';

@Injectable()
export class PatientsService {
  constructor(@InjectModel('Patient') private patientService: Model<Patient>) {}

  async create(patientData: Patient): Promise<Patient> {
    const createdPatient = await this.patientService.create(patientData);
    return createdPatient;
  }

  async findOne(id: string): Promise<Patient> {
    return this.patientService.findById(id).exec();
  }

  async findAll(): Promise<Patient[]> {
    const results = await this.patientService.find();

    return results.map((result, i) => {
      return {
        _id: result._id,
        id: i + 1,
        name: result.name,
        age: result.age,
        cancerStage: result.cancerStage,
        treatmentStart: result.treatmentStart,
        product: result.product,
      };
    });
  }
}
