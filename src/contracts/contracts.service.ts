import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Contract, ContractResponse, ContractStatus } from './contracts.model';
import { Patient } from 'src/patients/patients.model';
import { Hospital } from 'src/hospitals/hospitals.model';

@Injectable()
export class ContractsService {
  constructor(
    @InjectModel('Contract') private readonly contractService: Model<Contract>,
    @InjectModel('Patient') private patientService: Model<Patient>,
  ) {}

  async create(contractData: Contract): Promise<Contract> {
    const patientId = contractData.patient;
    const patient = await this.patientService.findById(patientId);

    if (patient.age > 55) {
      throw new BadRequestException('Patient must be between ages 0 and 55');
    }

    if (
      !['Stage 0', 'Stage 1', 'Stage 2', 'Stage 3'].includes(
        patient.cancerStage,
      )
    ) {
      throw new BadRequestException(
        "Patient's cancer stage is above the drug's specification",
      );
    }

    const createdContract = new this.contractService(contractData);
    return createdContract.save();
  }

  async findOne(id: string): Promise<ContractStatus> {
    const result = await this.contractService
      .findById(id)
      .populate('patient', 'name')
      .populate('hospital', 'name')
      .exec();

    const basePrices = {
      'ABC vial 10mg/ml': [
        {
          label: '10 Units',
          value: 1000,
          currency: 'CHF',
        },
        {
          label: '20 Units',
          value: 1800,
          currency: 'CHF',
        },
        {
          label: '30 Units',
          value: 2500,
          currency: 'CHF',
        },
      ],
      'ABC vial 20mg/ml': [
        {
          label: '10 Units',
          value: 1500,
          currency: 'CHF',
        },
        {
          label: '20 Units',
          value: 2700,
          currency: 'CHF',
        },
        {
          label: '30 Units',
          value: 4100,
          currency: 'CHF',
        },
      ],
    };

    const calculateAmountPayable = (percent) => {
      const product = basePrices[result.product];
      const findBasePrice = product.find(
        (unit) => unit.label === result.packSize,
      );
      const basePrice = findBasePrice.value;
      const amountPayable = (basePrice * percent) / 100;

      return amountPayable;
    };

    return {
      hospital: result.hospital.name,
      patient: result.patient.name,
      product: result.product,
      basePrice: result.basePrice,
      packSize: result.packSize,
      currency: result.currency || 'CHF',
      treatmentDuration: result.treatmentDuration,
      osPayable: calculateAmountPayable(result.osPercentage),
      osRefundable: calculateAmountPayable(100 - result.osPercentage),
      noOsPayable: calculateAmountPayable(result.noOsPercentage),
      noOsRefundable: calculateAmountPayable(100 - result.noOsPercentage),
      pfsPayable: calculateAmountPayable(result.pfsPercentage),
      pfsRefundable: calculateAmountPayable(100 - result.pfsPercentage),
      noPfsPayable: calculateAmountPayable(result.noPfsPercentage),
      noPfsRefundable: calculateAmountPayable(100 - result.noPfsPercentage),
    };
  }

  async findAll(): Promise<ContractResponse[]> {
    const results = await this.contractService
      .find()
      .populate('patient', 'name')
      .populate('hospital', 'name')
      .exec();

    return results.map((result, i) => {
      return {
        _id: result._id,
        contractId: `${i + 1}`,
        hospital: result.hospital?.name || '---',
        patient: result.patient?.name,
        product: result.product,
        packSize: result.packSize,
        basePrice: result.basePrice,
        // currency: result.currency,
        treatmentDuration: result.treatmentDuration,
        osPercentage: result.osPercentage,
        noOsPercentage: result.noOsPercentage,
        pfsPercentage: result.pfsPercentage,
        noPfsPercentage: result.noPfsPercentage,
      };
    });
  }
}
