import { Test } from '@nestjs/testing';
import * as dotenv from 'dotenv';
import { MongooseModule } from '@nestjs/mongoose';
import { ContractsController } from './contracts.controller';
import { ContractsService } from './contracts.service';
import { ContractSchema } from './contracts.model';
import { PatientSchema } from '../patients/patients.model';
import { PatientsModule } from '../patients/patients.module';

dotenv.config({ path: '.env' });

describe('ContractsController', () => {
  let contractsController: ContractsController;
  let contractsService: ContractsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({
          useFactory: () => ({
            uri: process.env.MONGO_DB_URI,
            connectionFactory: (connection) => {
              connection.plugin(require('mongoose-autopopulate'));
              return connection;
            },
          }),
        }),
        MongooseModule.forFeature([
          { name: 'Contract', schema: ContractSchema },
          { name: 'Patient', schema: PatientSchema },
        ]),
        PatientsModule,
      ],
      controllers: [ContractsController],
      providers: [ContractsService],
    }).compile();

    contractsService = moduleRef.get<ContractsService>(ContractsService);
    contractsController =
      moduleRef.get<ContractsController>(ContractsController);
  });

  describe('createContract', () => {
    it('should call contractsService.create with the request body', async () => {
      const req = {
        body: {
          hospital: 'Company A',
          patient: 'Company B',
          product: 'ABC',
          packSize: '10 Units',
          treatmentDuration: '1 Month',
          osPercentage: 75,
          noOsPercentage: 30,
          pfsPercentage: 85,
          noPfsPercentage: 40,
        },
      };
      const createdContract = {
        hospital: '64c3f5e93afac7a55780a26d',
        patient: '64c418a120087d124aa441e9',
        product: 'ABC',
        currency: 'CHF',
        packSize: '10 Units',
        treatmentDuration: '1',
        osPercentage: 75,
        noOsPercentage: 30,
        pfsPercentage: 85,
        noPfsPercentage: 40,
        _id: '64c59f09df859779f9733772',
        __v: 0,
      };
      jest.spyOn(contractsService, 'create').mockResolvedValue(createdContract);

      const result = await contractsController.createContract(req);

      expect(contractsService.create).toHaveBeenCalledWith(req.body);
      expect(result).toBe(createdContract);
    });
  });

  describe('getContracts', () => {
    it('should call contractsService.findAll', async () => {
      const allContracts = [
        {
          hospital: '64c3f5e93afac7a55780a26d',
          patient: '64c418a120087d124aa441e9',
          product: 'ABC',
          currency: 'CHF',
          packSize: '10 Units',
          treatmentDuration: '1 Month',
          osPercentage: 75,
          noOsPercentage: 30,
          pfsPercentage: 85,
          noPfsPercentage: 40,
          _id: '64c59f09df859779f9733772',
          __v: 0,
        },
      ];
      jest.spyOn(contractsService, 'findAll').mockResolvedValue(allContracts);

      const result = await contractsController.getContracts();

      expect(contractsService.findAll).toHaveBeenCalled();
      expect(result).toBe(allContracts);
    });
  });

  describe('getContract', () => {
    it('should call contractsService.findOne with the provided id', async () => {
      const contractId = 1;
      const foundContract = {
        hospital: '64c3f5e93afac7a55780a26d',
        patient: '64c418a120087d124aa441e9',
        product: 'ABC',
        currency: 'CHF',
        basePrice: '1800',
        packSize: '10 Units',
        treatmentDuration: '1 Month',
        osPercentage: 75,
        noOsPercentage: 30,
        pfsPercentage: 85,
        noPfsPercentage: 40,
        _id: '64c59f09df859779f9733772',
        __v: 0,
      };

      jest.spyOn(contractsService, 'findOne').mockResolvedValue(foundContract);

      const result = await contractsController.getContract({ id: contractId });

      expect(contractsService.findOne).toHaveBeenCalledWith(contractId);
      expect(result).toBe(foundContract);
    });
  });
});
