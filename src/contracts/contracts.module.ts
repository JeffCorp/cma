import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ContractsService } from './contracts.service';
import { ContractsController } from './contracts.controller';
import { ContractSchema } from './contracts.model';
import { PatientsModule } from 'src/patients/patients.module';
import { PatientSchema } from 'src/patients/patients.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Contract', schema: ContractSchema },
      { name: 'Patient', schema: PatientSchema },
    ]),
    PatientsModule,
  ],
  controllers: [ContractsController],
  providers: [ContractsService],
})
export class ContractsModule {}
