import { Controller, Get, Param, Post, Request } from '@nestjs/common';
import { ContractsService } from './contracts.service';

@Controller('contracts')
export class ContractsController {
  constructor(private contractsService: ContractsService) {}

  @Post()
  async createContract(@Request() req) {
    return this.contractsService.create(req.body);
  }

  @Get()
  async getContracts() {
    return this.contractsService.findAll();
  }

  @Get(':id')
  async getContract(@Param() param) {
    return this.contractsService.findOne(param.id);
  }
}
