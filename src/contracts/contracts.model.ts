// contract.schema.ts
import * as mongoose from 'mongoose';
import { Hospital } from 'src/hospitals/hospitals.model';
import { Patient } from 'src/patients/patients.model';

export const ContractSchema = new mongoose.Schema<ContractResponse>({
  contractId: { type: String },
  hospital: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Hospital',
    required: true,
  },
  patient: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Patient',
    required: true,
  },
  product: { type: String, required: true },
  currency: { type: String, default: 'CHF' },
  packSize: { type: String, required: true },
  treatmentDuration: { type: String, required: true },
  osPercentage: { type: Number, required: true },
  noOsPercentage: { type: Number, required: true },
  pfsPercentage: { type: Number, required: true },
  noPfsPercentage: { type: Number, required: true },
});

export interface Contract {
  _id?: string;
  contractId?: string;
  hospital: any;
  patient: any;
  product: string;
  basePrice?: string;
  currency?: string;
  packSize: string;
  treatmentDuration: string;
  osPercentage: number;
  noOsPercentage: number;
  pfsPercentage: number;
  noPfsPercentage: number;
}

export interface ContractResponse {
  contractId?: string;
  hospital: any;
  patient: any;
  product: string;
  basePrice?: string;
  currency?: string;
  packSize: string;
  treatmentDuration: string;
  osPercentage: number;
  noOsPercentage: number;
  pfsPercentage: number;
  noPfsPercentage: number;
}

export interface ContractStatus {
  hospital: string;
  patient: string;
  product: string;
  basePrice: string;
  packSize: string;
  currency: string;
  treatmentDuration: string;
  osPayable?: number;
  osRefundable?: number;
  noOsPayable?: number;
  noOsRefundable?: number;
  pfsPayable?: number;
  pfsRefundable?: number;
  noPfsPayable?: number;
  noPfsRefundable?: number;
}
